const MOVIES_BACKEND_URL = "http://localhost:3000/movies";
const FAV_MOVIES_BACKEND_URL = "http://localhost:3000/favourites";
let moviesList = [];
let moviesListFromBackend = [];
let favMoviesListFromBackend = [];
let favMoviesList = [];
let errorMessage;

function getMovies() {
    getMoviesFromBackend().then(moviesListResponse => {
        moviesListFromBackend = moviesListResponse;
        moviesList = moviesListFromBackend;
        getFavouriteMoviesFromBackend().then(favMoviesListResponse => {
            favMoviesListFromBackend = favMoviesListResponse;
            console.log('Fav Movies List From Backend', favMoviesListFromBackend);
            displayMoviesToHtml(moviesList);
        })

    })

}

function getMoviesFromBackend() {
    return fetch(MOVIES_BACKEND_URL).then(response => {
        if (response.ok) {
            return response.json();
        } else {
            return Promise.reject("Some Internal Error Occured");
        }
    }).then(moviesListResponse => {
        return moviesListResponse;
    }).catch(error => {
        errorMessage = error.message;
    })
}

function getFavouriteMoviesFromBackend() {
    return fetch(FAV_MOVIES_BACKEND_URL).then(response => {
        if (response.ok) {
            return response.json();
        } else {
            return Promise.reject("Some Internal Error Occured");
        }
    }).then(favMoviesListResponse => {
        return favMoviesListResponse;
    }).catch(error => {
        // errorMessage = error.message;
    })
}

function displayMoviesToHtml(moviesList) {
    let moviesListEle = document.getElementById("moviesContainer");
    moviesListEle.innerHTML = "";
    let moviesListEleString = "";
    let count = 1;
    moviesList.forEach(movie => {
        let isFavourite = isFavouriteMovie(movie);
        console.log("isFavourite", isFavourite);

        if (isFavourite) {
            moviesListEleString += `
        <div class="movie-card movie-id-${count++}" >
            <img src="${movie.posterImageUrl}" class="card-img-top movie-poster" alt="image not found">
            <div class="card-body">
                <section class="movie-title-favourite">
                <h5 class="card-title movie-title">${movie.title}</h5>
                <i class="fa fa-heart favourite-movie-icon" onclick="unFavouriteMovie(${movie.id})"></i>
                </section>
                <p class="card-text movie-description">${movie.description}</p>
                <section class="movie-info">
                     <section class="director-info">
                        <i class="fa fa-user"></i>
                        <span class="director-name">${movie.director}</span>
                     </section>
                     <section class="rating-section">
                        <i class="fa fa-star movie-rating-icon"></i>
                        <span class="movie-rating">${movie.rating}</span>
                    </section>
                </section>
            </div>

        </div>        
        `;
        } else {
            moviesListEleString += `
        <div class="movie-card movie-id-${count++}" >
            <img src="${movie.posterImageUrl}" class="card-img-top movie-poster" alt="image not found">
            <div class="card-body">
                <section class="movie-title-favourite">
                <h5 class="card-title movie-title">${movie.title}</h5>
                <i class="fa fa-heart favourite-icon" onclick="favouriteMovie(${movie.id})"></i>
                </section>
                <p class="card-text movie-description">${movie.description}</p>
                <section class="movie-info">
                     <section class="director-info">
                        <i class="fa fa-user"></i>
                        <span class="director-name">${movie.director}</span>
                     </section>
                     <section class="rating-section">
                        <i class="fa fa-star movie-rating-icon"></i>
                        <span class="movie-rating">${movie.rating}</span>
                    </section>
                </section>
            </div>

        </div>        
        `
        }

    });
    moviesListEle.innerHTML = moviesListEleString;
}

function filterMovie() {
    moviesList = moviesListFromBackend;
    let filterMovieName = document.getElementById('filterMovieName').value;
    console.log("moviesList", moviesList);
    console.log("filterMovieName", filterMovieName);
    moviesList = moviesList.filter(movie => movie.title.toLowerCase().includes(filterMovieName.toLowerCase()));
    displayMoviesToHtml(moviesList);
    document.getElementById('filterMovieName').value = "";
}

function displayAllMovies() {
    moviesList = moviesListFromBackend;
    displayMoviesToHtml(moviesList);
}

function favouriteMovie(movieId) {
    let movieToBeFavourited = moviesListFromBackend.find(movie => movie.id === movieId);
    console.log("Movie To Be Favourited", movieToBeFavourited);
    updateFavouriteMovieToBackend(movieToBeFavourited).then(addedFavouiteMovie => {
        console.log("Added Favourite Movie", addedFavouiteMovie);
        if (addedFavouiteMovie != null | addedFavouiteMovie !== "") {
            updateFavIcon(movieId);
            alert('Movie is added to favourites..');
        }
    }, error => {
        if (error.message === "exists") {
            alert("Movie is already added to favourites");
        }
    })
}

function updateFavouriteMovieToBackend(movieToBeFavourited) {
    return fetch(FAV_MOVIES_BACKEND_URL, {
        method: "POST",
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(movieToBeFavourited)
    }).then(response => {
        if (response.ok) {
            return response.json();
        } else if (response.status == 500) {
            return Promise.error("exists");
        } else {
            return Promise.reject("Some internal error occured..");
        }
    })
}

function updateFavIcon(movieId) {
    let movieEle = document.getElementsByClassName(`movie-id-${movieId}`)[0];
    let favIconEle = movieEle.getElementsByClassName('favourite-icon')[0];
    favIconEle.style.backgroundColor = '#ff4081';
}

function removeFavIcon(movieId) {
    let movieEle = document.getElementsByClassName(`movie-id-${movieId}`)[0];
    let favIconEle = movieEle.getElementsByClassName('favourite-movie-icon')[0];
    favIconEle.style.backgroundColor = '#fff';
}

function isFavouriteMovie(movieToBeTested) {
    let favMovie = favMoviesListFromBackend.find(movie => movie.id === movieToBeTested.id);
    console.log("FavMovie", favMovie);
    if (favMovie !== undefined) {
        return true;
    } else {
        return false;
    }
}

function unFavouriteMovie(movieId) {
    removeFavouriteMovieFromBackend(movieId).then(response => {
        console.log("Remove Favourited ", response);
        removeFavIcon(movieId);
        alert('Movie is removed from favourites..');

    })
}

async function removeFavouriteMovieFromBackend(movieId) {
    const response = await fetch(`${FAV_MOVIES_BACKEND_URL}/${movieId}`, {
        method: "DELETE"
    });
    if (response.ok) {
        return response.json();
    }
    else {
        return Promise.reject("Some internal error occured..");
    }
}

function displayFavouriteMovies() {
  getFavouriteMoviesFromBackend().then(favouriteMovieResponse =>{
    favMoviesList = favouriteMovieResponse;
    displayMoviesToHtml(favMoviesList);
  })
}